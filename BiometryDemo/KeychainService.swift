//
//  KeychainService.swift
//  BiometryDemo
//
//  Created by Naveen Negi on 18/04/22.
//

import Foundation
import Security
import LocalAuthentication


func getBundleId()->String {
    
    if let kKeychainItemIdentifier: String = Bundle.main.bundleIdentifier {
        return kKeychainItemIdentifier
    }
    
    return ""
}

public class KeychainService: NSObject {
    

    class func saveDataInKeychain(key: String, value: String) {
        if let dataValue = value.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            
            let idBundle: String = getBundleId()
            var query: [String: Any] = [:]
            
            if key == kPassword {
                let accessControl = SecAccessControlCreateWithFlags(nil, kSecAttrAccessibleWhenUnlocked, [.biometryCurrentSet, .applicationPassword], nil)
                query[kSecAttrAccessControl as String] = accessControl
            }
                query[kSecClass as String] = kSecClassGenericPassword
                query[kSecAttrAccount as String] = key
                query[kSecValueData as String] = dataValue
                query[kSecAttrGeneric as String] = idBundle
                
                SecItemDelete(query as CFDictionary)
                
                // Add the new keychain item
                var item: AnyObject?
                let status = SecItemAdd(query as CFDictionary, &item)
                if status == errSecAuthFailed {
                    print("\(key) status : errSecAuthFailed...............")
                    return
                }
                if (status != errSecSuccess) {    // Always check the status
                    if let err = SecCopyErrorMessageString(status, nil) {
                        print("Write failed: \(err)")
                    }
                } else if status == errSecSuccess {
                    print("\(key) : saved successfully in the keychain")
                }
            }
    }
    
    class func retrieveDataFromKeychain(key:String) -> String? {
        // Instantiate a new default keychain query
        // Tell the query to return a result
        // Limit our results to one item
        
       // let idBundle: String = getBundleId()
        
        var query: [String: Any] = [:]
        query[kSecClass as String] = kSecClassGenericPassword
        query[kSecAttrAccount as String] = key
        query[kSecMatchLimit as String] = kSecMatchLimitOne
       // query[kSecAttrGeneric as String] = idBundle
        query[kSecReturnData as String] = kCFBooleanTrue
        
        var dataTypeRef :AnyObject?
        
        // Search for the keychain items
        let status: OSStatus = SecItemCopyMatching(query as CFDictionary, &dataTypeRef)
        var contentsOfKeychain: String?
        
        if status == errSecSuccess {
            if let retrievedData = dataTypeRef as? Data {
                contentsOfKeychain = String(data: retrievedData, encoding: String.Encoding.utf8)
            }
        } else if status == errSecAuthFailed {
            print("errSecAuthFailed: \(status)")
            contentsOfKeychain = "\(status)"
        } else if status == errSecInvalidItemRef {
            //print("errSecInvalidItemRef: \(status)")
            contentsOfKeychain = "errSecInvalidItemRef: \(status)"
        } else if status == errSecItemNotFound {
           // print("\(key) was not retrieved from the keychain. Status code \(status)")
            contentsOfKeychain = "\(key) was not retrieved from the keychain Status code: \(status)"
        } else {
            //print("else: \(status)")
            contentsOfKeychain = "else: \(status)"
        }
        
        return contentsOfKeychain
    }
    
    
    
    private func resetKeyChain() {
        // delete keychain items for sunlife app
        let items = KeychainService.getAllKeyChainItemsOfClass(kSecClassGenericPassword as String)
        for item in items {
            let key = item.key
            KeychainService.deleteKeyChainItemForKey(key, typeOfItemClass: kSecClassGenericPassword)
        }
    }
    
    open class func getAllKeyChainItemsOfClass(_ secClass: String) -> [String: String] {
        
        let query: [String: Any] = [
            kSecClass as String: secClass,
            kSecReturnData as String: kCFBooleanTrue as Any,
            kSecReturnAttributes as String: kCFBooleanTrue as Any,
            kSecReturnRef as String: kCFBooleanTrue as Any,
            kSecMatchLimit as String: kSecMatchLimitAll
        ]
        
        var result: AnyObject?
        
        let lastResultCode = withUnsafeMutablePointer(to: &result) {
            SecItemCopyMatching(query as CFDictionary, UnsafeMutablePointer($0))
        }
        
        var values = [String: String]()
        if lastResultCode == noErr {
            let array = result as? Array<Dictionary<String, Any>>
            
            for item in array! {
                if let key = item[kSecAttrAccount as String] as? String,
                    let value = item[kSecValueData as String] as? Data {
                    values[key] = String(data: value, encoding: .utf8)
                }
            }
        }
        return values
    }
    
    @discardableResult open class func deleteKeyChainItemForKey(_ key: String, typeOfItemClass: CFString) -> Bool {
         let query = [
             (kSecClass as String): typeOfItemClass, //kSecClassGenericPassword,
             (kSecAttrAccount as String): key
         ] as [String: Any]
         
         return SecItemDelete(query as CFDictionary) == noErr
     }
}



















//
//
//    class func removeDataFromKeychain(service: String, account:String) {
//
//        // Instantiate a new default keychain query
//        let keychainQuery: NSMutableDictionary = NSMutableDictionary(objects: [kSecClassGenericPasswordValue, service, account, kCFBooleanTrue as Any], forKeys: [kSecClassValue, kSecAttrServiceValue, kSecAttrAccountValue, kSecReturnDataValue])
//
//        // Delete any existing items
//        let status = SecItemDelete(keychainQuery as CFDictionary)
//        if (status != errSecSuccess) {
//            if let err = SecCopyErrorMessageString(status, nil) {
//                print("Remove failed: \(err)")
//            }
//        }
//
//    }
    

//    class func updateDataInKeychain(key:String, value: String) {
//        if let valueString: Data = value.data(using: String.Encoding.utf8, allowLossyConversion: false) {
//
//            var query: [String: Any] = [:]
//            query[kSecClass as String] = kSecClassGenericPassword
//            query[kSecAttrAccount as String] = key
//            query[kSecAttrGeneric as String] = getBundleId()
//
//            let status = SecItemUpdate(query as CFDictionary, [kSecValueData:valueString] as CFDictionary)
//
//            if status == errSecAuthFailed {
//                if let err = SecCopyErrorMessageString(status, nil) {
//                    print("errSecAuthFailed: \(status)")
//                }
//
//            } else if (status != errSecSuccess) {
//                if let err = SecCopyErrorMessageString(status, nil) {
//                    print("Read failed: \(err)")
//                }
//            }
//        }
//    }
