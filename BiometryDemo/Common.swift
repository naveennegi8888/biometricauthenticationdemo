//
//  Common.swift
//  BiometryDemo
//
//  Created by Naveen Negi on 18/04/22.
//

import Foundation
import UIKit

let kUsername = "username"
let kPassword = "password"
let kIsBiometricEnabled = "isBiometricEnabled"


func showAlertInViewController(_ viewController: UIViewController?, title: String?, message: String?, ok: String?, cancel: String?, isOkFirst: Bool, okHandler: ((UIAlertAction) -> Void)?, cancelHandler: ((UIAlertAction) -> Void)?) {
    
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: UIAlertController.Style.alert)
    
    if isOkFirst {
        if ok != nil {
            alert.addAction(UIAlertAction(title: ok, style: UIAlertAction.Style.default, handler: okHandler))
        }
        
        if cancel != nil {
            alert.addAction(UIAlertAction(title: cancel, style: UIAlertAction.Style.default, handler: cancelHandler))
        }
    } else {
        if cancel != nil {
            alert.addAction(UIAlertAction(title: cancel, style: UIAlertAction.Style.default, handler: cancelHandler))
        }
        if ok != nil {
            alert.addAction(UIAlertAction(title: ok, style: UIAlertAction.Style.default, handler: okHandler))
        }
    }
    
    if viewController?.navigationController != nil {
        viewController?.present(alert, animated: true, completion: nil)
    } else {
        
        UIApplication.shared.keyWindow?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

func dismissAlertController() {
    guard let viewController = UIApplication.shared.keyWindow?.rootViewController  else {
        return
    }
    
    if viewController is UINavigationController {
        guard let presentedVC = viewController.presentedViewController, presentedVC is UIAlertController else {
            return
        }
        presentedVC.dismiss(animated: true, completion: nil)
    } else if  viewController is UIAlertController {
        viewController.dismiss(animated: true, completion: nil)
    }
    
}


//MARK:- show status popup to the user.....................
// func showErrorPop(_ msg: String, err: String?, vc: UIViewController) {
//    let alert = UIAlertController(title: msg, message: err, preferredStyle: .alert)
//
//    let cancelAction = UIAlertAction(title: "OK",style: .cancel, handler: nil)
//    alert.addAction(cancelAction)
//    vc.present(alert, animated: true, completion: nil)
//}
