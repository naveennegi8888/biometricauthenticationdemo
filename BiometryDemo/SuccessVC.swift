//
//  SuccessVC.swift
//  BiometryDemo
//
//  Created by Naveen Negi on 18/04/22.
//

import UIKit
import LocalAuthentication

class SuccessVC: UIViewController {

    @IBOutlet weak var switchBtn: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSwitchBtnStatus()
    }
    
    
    func showSwitchBtnStatus() {
        if let _ = UserDefaults.standard.value(forKey: kIsBiometricEnabled) {
            switchBtn.isOn = true
        } else {
            switchBtn.isOn = false
        }
    }
}


//MARK:- IBActions.........................
extension SuccessVC {
    
    @IBAction func switchBtnAction(sender: UISwitch) {
        
        let context = LAContext()
        let biometricBool = context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil)
        if biometricBool {
            if sender.isOn {
                sender.isOn = true
                UserDefaults.standard.setValue("true", forKey: kIsBiometricEnabled)
            } else {
                sender.isOn = false
                UserDefaults.standard.setValue(nil, forKey: kIsBiometricEnabled)
            }
        } else {
            sender.isOn = false
            UserDefaults.standard.setValue(nil, forKey: kIsBiometricEnabled)
            showAlertInViewController(self, title: "FaceID is disable from the settings",
                                                      message: "Please enable faceID from the settings",
                                                      ok: "Ok",
                                                      cancel: nil, isOkFirst: true, okHandler: nil,
                                                      cancelHandler: nil)
        }
    }
}

