//
//  ViewController.swift
//  BiometryDemo
//
//  Created by Naveen Negi on 18/04/22.
//

import UIKit
import LocalAuthentication


class LoginVC: UIViewController {

    @IBOutlet weak var usernameTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var keychainStatusLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        passwordTF.text = ""
        keychainStatusLabel.text = ""
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        getCredentailsFromKeychain()
    }

}

//MARK:- private Functions....................
extension LoginVC {
    
    //MARK:- get saved data from the keychain..............
    private func getCredentailsFromKeychain() {
            
        if let username = KeychainService.retrieveDataFromKeychain(key: kUsername) {
            usernameTF.text = username
        }
            
           
        let context = LAContext()
        let biometricBool = context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: nil)
        
        if let _ = UserDefaults.standard.value(forKey: kIsBiometricEnabled), biometricBool {
            if let password = KeychainService.retrieveDataFromKeychain(key: kPassword) {
                if password == "-25293" {
                    showAlertInViewController(self, title: "We noticed you recently updated faceID on your device",
                                                              message: "To protect your accounts, we need to update your faceID settings for the app.\nPlease sign in with your password to continue",
                                                              ok: "Sign In",
                                                              cancel: nil, isOkFirst: true, okHandler: nil,
                                                              cancelHandler: nil)
                } else {
                    
                    passwordTF.text = password
                    self.authenticateUser()
                }
                
                keychainStatusLabel.text = password
            }
        }
    }
    
    
    private func navigateToSuccessVC() {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        guard let vc = storyboard.instantiateViewController(withIdentifier: "SuccessVC") as? SuccessVC else {
            return
        }
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
//MARK:- show status popup to the user.....................
    private func notifyUser(_ msg: String, err: String?) {
        let alert = UIAlertController(title: msg, message: err, preferredStyle: .alert)

        let cancelAction = UIAlertAction(title: "OK",style: .cancel, handler: nil)
        alert.addAction(cancelAction)

        self.present(alert, animated: true, completion: nil)
    }
}


//MARK:- IBActions.......................
extension LoginVC {
    @IBAction func loginBtnClicked(){
        passwordTF.resignFirstResponder()
        
        guard let password = passwordTF.text, let username = usernameTF.text, password != "", username != "" else {
            notifyUser("Alert!!", err: "Please enter username & password")
            return
        }
        
        KeychainService.saveDataInKeychain(key: "username", value: username)
        KeychainService.saveDataInKeychain(key: kPassword, value: password)
        
        passwordTF.text = ""
        navigateToSuccessVC()
        
    }
}


// MARK:- authenticateUser using biometric............
extension LoginVC {
    func authenticateUser(){
        let context = LAContext()
            
            var error: NSError?
            
            if context.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,error: &error) {
                        
                // Device can use biometric authentication
                context.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics,localizedReason: "Access requires authentication",reply: { [weak self] (success, error) in
                        
                    DispatchQueue.main.async {
                            
                            if let err = error {
                                
                                switch err._code {
                                    
                                case LAError.Code.systemCancel.rawValue:
                                    self?.notifyUser("Session cancelled",
                                                    err: err.localizedDescription)
                                    
                                case LAError.Code.userCancel.rawValue:
                                    self?.notifyUser("Please try again",
                                                    err: err.localizedDescription)
                                    
                                case LAError.Code.userFallback.rawValue:
                                    self?.notifyUser("Authentication",
                                                    err: "Password option selected")
                                    // Custom code to obtain password here
                                case LAError.Code.invalidContext.rawValue:
                                    self?.notifyUser("invalidContext",
                                                    err: "")
                                case LAError.Code.authenticationFailed.rawValue:
                                    self?.notifyUser("authenticationFailed",
                                                    err: "")
                                case LAError.Code.userFallback.rawValue:
                                    self?.notifyUser("userFallback",
                                                    err: "")
                                default:
                                    self?.notifyUser("Default Error",
                                                    err: err.localizedDescription)
                                }
                                
                            } else {
                                self?.navigateToSuccessVC()
                                self?.notifyUser("Authentication Successful",
                                                err: "You now have full access")
                                
                            }
                        }
                })

            } else {
                // Device cannot use biometric authentication
                if let err = error {
                    switch err.code {
                        
                    case LAError.Code.biometryNotEnrolled.rawValue:
                        notifyUser("User is not enrolled",
                                   err: err.localizedDescription)
                        
                    case LAError.Code.passcodeNotSet.rawValue:
                        notifyUser("A passcode has not been set",
                                   err: err.localizedDescription)
                        
                        
                    case LAError.Code.biometryNotAvailable.rawValue:
                        notifyUser("Biometric authentication not available",
                                   err: err.localizedDescription)
                    default:
                        notifyUser("Unknown error",
                                   err: err.localizedDescription)
                    }
                }
            }
    }
}

