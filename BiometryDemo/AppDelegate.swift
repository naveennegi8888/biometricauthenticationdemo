//
//  AppDelegate.swift
//  BiometryDemo
//
//  Created by Naveen Negi on 18/04/22.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
//        resetKeyChain()
//        resetDefaults()
        
        return true
    }

    private func resetKeyChain() {
        // delete keychain items for sunlife app
        let items = KeychainService.getAllKeyChainItemsOfClass(kSecClassGenericPassword as String)
        for item in items {
            let key = item.key
            KeychainService.deleteKeyChainItemForKey(key, typeOfItemClass: kSecClassGenericPassword)
        }
    }
    
    private func resetDefaults() {
        // delete UserDefaults for sunlife app
        let defaultsName = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: defaultsName)
    }
    
    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

